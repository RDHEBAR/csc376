#!/usr/bin/python
# -*- coding: utf-8 -*-

import sleepy
import sys
import socket


def usage(script_name):
    print ('Usage: py', script_name, '<-l>', ' <port number>',
           '[<server address>]')


if __name__ == '__main__':

    isServer = isClient = False

    # get the command line arguments

    argc = len(sys.argv)
    hostname = socket.gethostname()
    try:

        if sys.argv[1] == '-l':
            #print 'Messenger.py is in SERVER MODE.'
            isServer = True
        else:

            #print 'Messenger.py is in CLIENT MODE.'
            isClient = True
    except Exception as e:

        print (e)
        sys.exit()

    if isServer:
        serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        serversocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR,
                                1)
        serversocket.bind(('', int(sys.argv[2])))  # binds to any available interface
        serversocket.listen(5)

    # wait for a connection and accept it

        (sock, addr) = serversocket.accept()

    # receive the first message

        message = sys.stdin.readline()  # read a message from standard input
        while message:
            sock.send(message.encode())
            return_msg = sock.recv(1024)
            if return_msg:
                print (return_msg.decode())
            else:
                print ('[NOTHING RETURNED!]')
            message = sys.stdin.readline()  # read another message
    elif isClient:

        # msg_bytes = sock.recv(1024)
        # while msg_bytes:
        #    sock.send(msg_bytes)  # send the message back
        #    msg_bytes = sock.recv(1024)  # get a new message
        # sock.close()  # close the socket
        # serversocket.close()  # close the server socket

        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        if argc == 3:
            server = sys.argv[2]
        else:
            server = 'localhost'
        sock.connect((server, int(sys.argv[1])))
        message = sys.stdin.readline()  # read a message from standard input
        while message:
            sock.send(message.encode())
            return_msg = sock.recv(1024)
            if return_msg:
                print (return_msg.decode())
            else:
                print ('[NOTHING RETURNED!]')
            message = sys.stdin.readline()  # read another message
        sock.close()

    # read a message from standard input

    # create a communicator object

    # sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # if argc == 3:
    #    server = sys.argv[2]
    # else:
    #    server = 'localhost'
    # sock.connect((server, int(sys.argv[1])))

    # transmit the message as bytes (which is what send/recv uses)

    print ('attempting to send message using utf-8 encoding ...')

    sock.close()

    # receive the return message from the server

    # TESTING!

    if return_msg:
        print ('message returned: ' + return_msg.decode())
    else:
        print ('NOTHING RETURNED!')


			
