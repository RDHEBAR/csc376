import os
import socket
import sys
import threading

#Setup begin
HOST = 'localhost'
PORT = ''
BYTE_BUF = 1024
isServer = isClient = False

if sys.argv[1] == '-l':
    isServer = True
    PORT = int(sys.argv[2])
else:
    isClient = True
    PORT = int(sys.argv[1])
#setup end

class send(threading.Thread):
    def __init__(self, sock):
        threading.Thread.__init__(self)
        self.sock = sock

    def run(self):
        message = sys.stdin.readline().strip('\n')
        while message:
            self.sock.send(message.encode())
            message = sys.stdin.readline().strip('\n')


class recv(threading.Thread):
    def __init__(self, sock):
        threading.Thread.__init__(self)
        self.sock = sock
        
    def run(self):
        msg_bytes = self.sock.recv(BYTE_BUF).decode()
        while msg_bytes:
            print(msg_bytes)
            msg_bytes = self.sock.recv(BYTE_BUF).decode()

def setup_server():
    if isServer:
        server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        server_socket.bind((HOST, PORT))
        server_socket.listen(2)
        (sock, addr) = server_socket.accept()
        return (sock, addr, server_socket)

    elif isClient:
        client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        client_socket.connect((HOST, PORT))
        return client_socket

def teardown(sock = None):
        if sock:
                sock.close()
        os._exit(0)
        sys.exit()

def setup_threads(sock):
        return (recv(sock), send(sock))

if isServer:
    (sock, addr, server_socket) = setup_server()
    (receiving_thread, sending_thread) = setup_threads(sock)

    receiving_thread.start()
    sending_thread.start()
    sending_thread.join()

    server_socket.close()
    teardown(sock)

elif isClient:
    sock = setup_server()
    (receiving_thread, sending_thread) = setup_threads(sock)

    receiving_thread.start()
    sending_thread.start()
    receiving_thread.join()
    teardown(sock)
