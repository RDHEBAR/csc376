BYTE_BUF = 2048
import sys, server_globals
CONN = ('localhost',int(sys.argv[1]))

def usage(script_name):
    print ('Usage: py ' + script_name + ' <port number>')

def getConnectPort(sock):
    global connect_port
    msg_bytes = sock.recv(BYTE_BUF)
    if len(msg_bytes):
        connect_port = msg_bytes.decode()
    else:
        sock.close()
        return None
    return connect_port    

def get_name(sock):
    request = 'What is your name?'
    sock.send(request.encode())
    name_bytes = sock.recv(BYTE_BUF)
    name = name_bytes.decode()
    return name
    
if __name__ == "__main__":
    
    import socket

    from msg_relay import MsgRelay
    argc = len(sys.argv)
    if argc != 2:
        usage(sys.argv[0])
        sys.exit()
        
    serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    serversocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    serversocket.bind(CONN)
    serversocket.listen(5)
    
    while True:
        sock, addr = serversocket.accept()
        #.keys = sockets
        #.values = port numbers
        connect_port = getConnectPort(sock)
        name_and_port = (get_name(sock), connect_port)
        server_globals.clients[sock] = name_and_port
        
        MsgRelay(sock).start()
        #FileRelay(sock).start()
        
