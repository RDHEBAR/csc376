import threading 
import os, sys
BYTE_BUF = 2048
class FileRequestListener (threading.Thread):
    def __init__(self,serversocket):
        threading.Thread.__init__(self)
        self.serversocket = serversocket
        
    def run(self):
        while True:
            try:
                sock, addr = self.serversocket.accept()
                user = sock.recv(BYTE_BUF).decode()
                filename = sock.recv(BYTE_BUF).decode()
                file_stat = os.stat(filename)
                if file_stat.st_size:
                    file = open(filename, 'rb')
                    while True:
                        file_bytes = file.read(1024)
                        if file_bytes:
                            sock.send(file_bytes)
                        else:
                            break
                    file.close()
                else:
                    pass
                sock.close()
            except OSError:
                pass
                