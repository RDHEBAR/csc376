import threading, sys, os, socket, server_globals
BYTE_BUF = 2048
class RetrieveFile(threading.Thread):
    def __init__(self, port, username, filename):
        threading.Thread.__init__(self)
        
        self.hostname = 'localhost'
        self.username = username
        self.port = int(port)
        #self.port = server_globals.clients[sock][1] does not work
        #requires sock to be passed in and that changes requestFile too much
        self.filename = filename
    
    def run(self):
        #print("RetrieveFile is now running.")
        
        new_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        CONN = (self.hostname,self.port)
        try:
            new_sock.connect(CONN)
            new_sock.send(self.username.encode())
            new_sock.send(self.filename.encode())
            
        except:
            sys.exit()
        file = open(self.filename, 'wb')
        while True:
            try:
                file_bytes = new_sock.recv(BYTE_BUF)
            except:
                break
            if len(file_bytes):
                file.write(file_bytes)
            else:
                new_sock.close()
                break
        file.close()
            