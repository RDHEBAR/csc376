import sys

argc= len(sys.argv)
o_exists = t_exists = h_exists = False
i = 1

while i < argc:
    if(sys.argv[i] == '-o'):
        o_exists = True
        o_string = 'option 1: ' + sys.argv[i+1]
     
    if(sys.argv[i] == '-t'):
        t_exists = True
        t_string = 'option 2: ' + sys.argv[i+1]
        
    if(sys.argv[i] == '-h'):
        h_exists = True
        h_string = 'option 3'
        
    i+=1

print("Standard Input:")
text = sys.stdin.readlines()
for line in text:
    line = line.rstrip()
    print(line)


print("Command line arguments:")
if o_exists and t_exists and h_exists:
    print(o_string+"\n"+t_string+"\n"+h_string)
elif not o_exists and t_exists and h_exists:
    print(t_string+"\n"+h_string)
elif not t_exists and o_exists and h_exists:
    print(o_string+"\n"+h_string)
elif not h_exists and o_exists and t_exists:
   print(o_string+"\n"+t_string)

elif o_exists and not t_exists and not h_exists:
    print(o_string)
elif t_exists and not o_exists and not h_exists:
    print(t_string)
elif h_exists and not o_exists and not t_exists:
   print(h_string)
