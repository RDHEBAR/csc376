import server_globals
BYTE_BUF = 2048
def displayMenu():
    print( "Enter an option ('m', 'f', 'x'):")
    print("\t  (M)essage (send)")
    print("\t  (F)ile (request)")
    print("\t e(X)it")
    

def usage(script_name):
    print ('Usage: py ' + script_name + ' -l <client listen number> -p <connect port number>')
  
def connectToServer(port, server):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    if server:
        sock.connect((server, int(port)))
    return sock
  
def createServerSocket(port):
    serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    serversocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    serversocket.bind(('', int(port)))
    serversocket.listen(5)
    return serversocket

def getOption():
    response = sys.stdin.readline().rstrip()
    if not response:
        return None
    return response
    
def send_name(sock):
    import server_globals
    
    print( 'Waiting for request from server...')
    msg_bytes = sock.recv(BYTE_BUF)
    print(msg_bytes.decode())
    name = sys.stdin.readline().rstrip()
    sock.send(name.encode())

def sendListeningPort(sock):
    global listen_port
    try:
        sock.send(listen_port.encode())
    except:
        return None
    return 1
    
def getConnectPort(sock):
    import server_globals
    global connect_port
    
    msg_bytes = sock.recv(BYTE_BUF)
    if len(msg_bytes):
        connect_port = msg_bytes.decode()
    else:
         sock.close()
         return None
    return 1
    
def requestFile():
    global connect_port
    port = connect_port
    print('Which user has the file?')
    user = sys.stdin.readline().rstrip()
    print('Which file do you want?')
    filename = sys.stdin.readline().rstrip()
    if not user:
        return None
    if not filename:
        return None
    RetrieveFile(port, user, filename).start()
    return 1


def sendMessage(sock):
    print("Enter your message")
    msg = sys.stdin.readline()
    if not msg:
        return None
    try:
        sock.send(msg.encode())
    except:
        return None
    return 1
  

if __name__ == "__main__":
    import sys, os
    import socket
    argc = len(sys.argv)
    if argc < 3 or argc > 5:
        usage(sys.argv[0])
        sys.exit()
        
    import getopt
    optlist, non_option_args = getopt.getopt(sys.argv[1:], 'l:p:')
    act_as_server = False
    listen_port = None
    connect_port = None
    for opt, arg in optlist:
        if opt == "-l":
            listen_port = arg
        if opt == "-p":
            connect_port = arg
    
    if not listen_port: #big red flag number 1
        usage(sys.argv[0])
        sys.exit()
        
    serversocket = createServerSocket(listen_port)
    if not connect_port: #big red flag number 2
        act_as_server = True
    
    if act_as_server: #big red flag number 3
        usage(sys.argv[0])
        sys.exit()
    
    else:
        sock = connectToServer(connect_port, 'localhost')
        
        if not sendListeningPort(sock): 
            serversocket.close()
            sys.exit()
    
    send_name(sock)
    from recv_messages import RecvMessages
    RecvMessages(sock).start()
    from file_request_listener import FileRequestListener
    FileRequestListener(serversocket).start()
    from retrieve_file import RetrieveFile
    
    while True:
        displayMenu()
        option = getOption()
        if not option:
            break
        if option == "m":
            if not sendMessage(sock):
                break
        elif option == 'f':
            if not requestFile():
                break
        elif option == 'x':
            break
        else:
            print("Please enter \'m\', \'f\', or \'x\'. ")
    
    try:
        sock.shutdown(socket.SHUT_WR)
        sock.close()
        serversocket.close()
        os._exit(0)
    except:
        pass