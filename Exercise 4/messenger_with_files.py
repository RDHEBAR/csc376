import os
import socket
import sys
import threading


#Setup begin
HOST = 'localhost'
PORT = ''
BYTE_BUF = 4096
isServer = isClient = False
THE_TERMINATOR = '#'
length = len(sys.argv)
try:
    if length == 3:
        isServer = True
        PORT = int(sys.argv[2])
    else:
        isClient = True
        PORT = int(sys.argv[4])
except IndexError:
    sys.exit()
#setup end

class send(threading.Thread):
    def __init__(self, sock):
        threading.Thread.__init__(self)
        self.sock = sock
    def run(self):
        while True:
            print("Enter an option ('m', 'f', 'x'):")
            print("\t (M)essage")
            print("\t (F)ile")
            print("\t e(X)it")
            choice = sys.stdin.readline().strip('\n')
            while choice:
                if(choice == "m" or choice == "M"):
                    print("Enter your message below")
                    message = sys.stdin.readline().strip('\n')
                    self.sock.send(message.encode())
                elif(choice == "f" or choice == "F"):
                    print("Which file do you want?")
                    filename = sys.stdin.readline().strip('\n') + str(THE_TERMINATOR)
                    
                    #print("I want", filename,"from the other side.")
                    #print(os.getcwd())
                    #os.chdir("client")
                    #f = open("Ameca_splendens.jpg", "w+")
                    #f.close()
                    #os.chdir('..')
                    self.sock.send(filename.encode())
                    #img = open(filename, 'rb').read()
                    #self.sock.sendall(img.encode())
                    
                elif(choice == 'x' or choice == 'X'):
                    os._exit(0)
                    sys.exit()
                break

    #def run(self):
    #    message = sys.stdin.readline().strip('\n')
    #    while message:
    #        self.sock.send(message.encode())
    #        message = sys.stdin.readline().strip('\n')


class recv(threading.Thread):
    def __init__(self, sock):
        threading.Thread.__init__(self)
        self.sock = sock
        
    def run(self):
        msg_bytes = self.sock.recv(BYTE_BUF).decode()
       # for each in msg_bytes:
    #        print(each)
        #print("msg_bytes:",msg_bytes)
        #print("msg_bytes[-1]:",msg_bytes[-1])
        
        while msg_bytes:
            if msg_bytes[-1] == THE_TERMINATOR:
                try:
                    filename = msg_bytes[0:(len(msg_bytes)-1)]
                #    print("Received", filename)
                #    print(os.listdir('.'))
                #    print(os.getcwd())
                    os.chdir("server")
                    f = open("one-liners.txt", "w+")
                   # print(os.getcwd())
                    filename = os.getcwd() + "/" + filename
                    print("Full path of file to send:",filename)
                    file_stat = os.stat(filename)
                    os.chdir('..')
                    #print(os.getcwd())
                    os.chdir("client")
                    f = open("Ameca_splendens.jpg", "w+")
                  
                    f.close()
                except FileNotFoundError:
                    file_exists = False
                    if(not file_exists) or (file_stat.st_size == 0):
                        print("Sending file does not exist.encode()")
                        sys.exit()

                #with open(filename, 'rb') as file:
                #    byte = file.read()
                #    while byte:
                #        self.sock.send(byte)
                    #print("filename: ",len(filename))
                    #print("type(filename): ",type(filename))
                    #print("msg_bytes: ",len(msg_bytes))
                    #print("type(msg_bytes): ",type(msg_bytes))
            else:
                print(msg_bytes)
                print("Enter an option ('m', 'f', 'x'):")
                print("\t (M)essage")
                print("\t (F)ile")
                print("\t e(X)it")
                msg_bytes = self.sock.recv(BYTE_BUF).decode()
            
        
        
            
            

def setup_server():
    if isServer:
        server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        server_socket.bind((HOST, PORT))
        server_socket.listen(2)
        (sock, addr) = server_socket.accept()
        return (sock, addr, server_socket)

    elif isClient:
        client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        client_socket.connect((HOST, PORT))
        return client_socket

def teardown(sock = None):
        if sock:
                sock.close()
        os._exit(0)
        sys.exit()

def setup_threads(sock):
        return (recv(sock), send(sock))

if isServer:
    (sock, addr, server_socket) = setup_server()
    (receiving_thread, sending_thread) = setup_threads(sock)

    receiving_thread.start()
    sending_thread.start()
    sending_thread.join()

    server_socket.close()
    teardown(sock)

elif isClient:

    sock = setup_server()
    (receiving_thread, sending_thread) = setup_threads(sock)

    receiving_thread.start()
    sending_thread.start()
    receiving_thread.join()
    teardown(sock)
