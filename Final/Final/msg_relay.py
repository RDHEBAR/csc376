import threading
import server_globals
BYTE_BUF = 2048
class MsgRelay(threading.Thread):
    def __init__(self, connection):
        threading.Thread.__init__(self)
        self.connection = connection
        self.name = server_globals.clients[connection][0]
        #self.connect_port = connect_port
    
        #values() = client usernames
        #keys() = their underlying sockets.
    
    def run(self):
        #self.get_name() #done already when client connects.
        while True:
            msg_bytes = self.connection.recv(BYTE_BUF)
            if len(msg_bytes):
                pass
            else:
                self.connection.close()
                del server_globals.clients[self.connection]
                break
            
            msg = str(self.name) + ': '
            for cur_connection in server_globals.clients:
                if cur_connection is not self.connection:
                    cur_connection.send(msg.encode())
                    cur_connection.send(msg_bytes)
        