#! python
BYTE_BUF = 2048
"""Recieves Messages from server."""
import threading
import os,sys

class RecvMessages (threading.Thread):
    def __init__(self,client_socket):
        threading.Thread.__init__(self)
        self.client_socket=client_socket
        
    def run(self):
        while True:
            try:
                msg_bytes= self.client_socket.recv(BYTE_BUF)
            except:
                sys.exit()
            if len(msg_bytes):
                pass
                print(msg_bytes.decode(), end= '')
            else:
                self.client_socket.close()
                os._exit(0)