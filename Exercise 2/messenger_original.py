#!/usr/bin/python
# -*- coding: utf-8 -*-

import sleepy
import sys
import socket


def usage(script_name):
    print ('Usage: py',script_name,'<-l>',' <port number>','[<server address>]')


if __name__ == '__main__':

    isServer = isClient = False
    # get the command line arguments

    argc = len(sys.argv)
    hostname = socket.gethostname()
    try:
        
        
        if sys.argv[1] == '-l':
           # print ('Messenger.py is in SERVER MODE.')
            isServer = True
            
        else:
            #print ('Messenger.py is in CLIENT MODE.')
            isClient = True
        
        
    except Exception as e:
        print(e)
        sys.exit()    

    if isServer:
        server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        port = sys.argv[2]
        server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        server.bind((hostname, int(port)))
        server.listen(1)
        sock, addr= server.accept()
        while True:
            message = sys.stdin.readline()
            sock.send(message.encode())
            msg_bytes= sock.recv(1024)
            
        
    elif isClient:
        port = sys.argv[1]
        client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        client.connect((hostname, int(port)))
        while True:
            message = sys.stdin.readline()
            client.send(message.encode())
            return_msg= client.recv( 1024 )
        
    # read a message from standard input
    
    

    # create a communicator object

    #sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    #if argc == 3:
    #    server = sys.argv[2]
    #else:
    #    server = 'localhost'
    #sock.connect((server, int(sys.argv[1])))

    # transmit the message as bytes (which is what send/recv uses)

    #print ('attempting to send message using utf-8 encoding ...')
    
    sock.close()
    # receive the return message from the server

    

    # TESTING!

    if return_msg:
        print (return_msg.decode())
    else:
        #print ('NOTHING RETURNED!')


			
